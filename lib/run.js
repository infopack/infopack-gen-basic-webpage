var Promise = require('bluebird');
/**
 * This is the function which effectivly performs the action.
 * @param {*} pipeline Pipeline instance
 * @param {*} settings
 * @returns Promise
 */
function run(pipeline, settings) {
	settings = settings || {};
			
	return Promise
		.resolve()
		.then(() => {
			// your code here
			return;
		});
}

module.exports = run;
